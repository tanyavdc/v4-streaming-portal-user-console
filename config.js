var config = {};

config.web = {};
config.pg = {};

//config.defaults; colours, language, etc 
config.pg.con_string = 'pg://postgres:Launch1234!!@localhost:5432/streaming-api-LOCAL';
config.web.port =  process.env.PORT || 8095;
config.default_language = 'en'; 

/* 
{
  "development": {
    "redisPort": 6379,
    "redisHost": "127.0.0.1",
    "errorHandlerOptions": {"dumpExceptions": true, "showStack": true}
  },
  "production": {
    "redisPort": 6379,
    "redisHost": "46.137.195.230",
    "errorHandlerOptions": {"dumpExceptions": false, "showStack": false}
  }
}
*/ 


module.exports = config;