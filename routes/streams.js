var express = require('express')
  , router = express.Router();

var model = require('../model/streams.js');
var cache = require('../model/cache');

var passport = require('passport');
var flash = require('connect-flash');

var path = require('path'); 

var security = require('../middleware/security.js');

var sess; 

// get stream by id 
router.get('/stream_by_id/:type/:id',security.ensureAuthenticated, function(req, res) {

  var sess = req.session; 

  var id = req.params.id; 
  var stream_type = req.params.type; 
  var current_language = sess.current_language; 
  var default_language = req.app.locals.default_language 

  var logged_in = sess.passport; 

  var user_id = sess.passport.user.id;  // null if account is dissabled 

  // holds stream thumbs for recently watched, popular streams, etc
  // to be populated in jade as a stream-thumb-menu
  var stream_previews = {}; 

  model.getStream(id,current_language,stream_type,function(err,stream){

    // if didnt get a stream
    console.log("the stream " + JSON.stringify(stream));
    if (!stream || err){
      res.render('404', { session:req.session, msg:"Stream DNE" });
      return;
    }

    console.log("in main route got stream data " + JSON.stringify(stream)); 
    stream_id = stream.id;

    // if stream is private and user is not logged in
    if (stream.private && !logged_in){
      res.redirect('/login');
      return;
    }
    // if stream is private, redirect to login
    // else 

    model.getCategoryLabelsForStream(stream_id,current_language,
      function(categories){
      console.log("the categories in route " + categories); 


      // get ads tagged to this stream

      model.getStreamAds(stream_id,current_language,function(err,ads){
       
        var ads = ads; 
        var ads_count = Object.keys(ads).length;

        if (ads_count == 0){
    
          // dummy ad so as to not break jade with ad.video reference 
         
          ads = [{id:'null',title:'null',video:'null'}];

        }
        
          model.getTagsForStream(stream_id,function(tags){
            
            model.checkIfFavourited(sess,user_id,stream_id,
              function(is_favourited){
             

              model.getRelatedStreams(stream_id,
                current_language,function(related_streams){ 

                  var menu = {title:'Related Streams',streams:related_streams}
                  stream_previews['Related Streams'] = menu;
                

                model.getVotes(sess,stream_id,user_id,function(user_vote,votes_total){
                

                  var file_type = getFileTypeForHTML(stream.video); 
                  // we just have one ad for now ** 
                  var ad_file_type = getFileTypeForHTML(ads[0].video); 
                 

                  res.render('stream-view', { title: stream_type,
                  stream: stream, stream_categories: categories,
                  stream_advertisements:ads,
                  ad_file_type:ad_file_type,
                  id:id,settings:req.app.locals.settings,
                  tags:tags,session:sess,favourited:is_favourited,
                  stream_previews:stream_previews,user_vote:user_vote,
                  votes:votes_total,file_type:file_type});

              }); 
            })
          })
        });
      }); 
    }); 
  });
});




// get streams by type 

router.get('/stream_by_type/:type', function(req, res) {


  var sess = req.session; 
  var current_language = sess.current_language; 

  var type_categories = getCachedCategoriesForType(sess,req.params.type); 

  if(serviceEnabled(req,req.params.type)){
    model.getAllStreamsOfType(req.params.type,current_language, function(err, results) {

        // if no stream entries for this language
        if (results == null || Object.keys(results).length == 0){
          console.log("no streams for lang " + current_language);
          console.log("the default lang in getstreams " + req.app.locals.default_language );

          // get stream info in default LANGUAGE
          model.getAllStreamsOfType(req.params.type,req.app.locals.default_language, 
            function(err, default_results) {


             res.render('streams-list', { title: req.params.type, 
            type: req.params.type, category:{id:null,label:'All'}, 
            categories: type_categories,streams: default_results,
            settings:req.app.locals.settings,
            session:sess});
        
          });

      }

      else{

         res.render('streams-list', { title: req.params.type, 
          type: req.params.type, category:{id:null,label:'All'}, 
          categories: type_categories,
          streams: results,settings:req.app.locals.settings,session:sess});
   
      };
  	});
  }

    // service is not available (i.e admin disabled vod content)
   else{
      res.render('404', { session:req.session, msg:"Sorry" });
    }
});


// get streams by category 

router.get('/stream_by_category/:type/:category', function(req, res) {
  
	
  var type_categories; 

  var sess = req.session;


  var current_language = sess.current_language;
  var default_language = req.app.locals.default_language;  

  var type_categories = getCachedCategoriesForType(sess,req.params.type);
  var category = req.params.category;   
  var category_label = type_categories[category];  

  model.getTypeStreamsOfCategory(req.params.type, current_language,category,
	function(err, results) {

    // if no stream entries for this language
    if (results == null || Object.keys(results).length == 0){
      console.log("no streams for lang " + current_language);
      console.log("the default lang in getstreams " + req.app.locals.default_language)

      // get stream info in default LANGUAGE
      model.getTypeStreamsOfCategory(req.params.type, 
        default_language, category,function(err, default_results) {
          res.render('streams-list', { title: req.params.type,
          type: req.params.type,category:{id:category,label:category_label}, 
          categories: type_categories,streams: default_results,
          settings:req.app.locals.settings,session:sess});
        });
      }

    else{

     res.render('streams-list', { title: req.params.type, 
      type: req.params.type,category:{id:category,label:category_label},
      categories: type_categories,streams: results, 
      settings:req.app.locals.settings,session:sess});         
      }    	    
    });
});


router.get('/' , function(req, res) {
	 
  
  var sess = req.session; 

  var language = sess.current_language;

  // holds stream thumbs for recently watched, popular streams, etc
  // to be populated in jade as a stream-thumb-menu 
  var stream_previews = {} 
  
  model.getFeaturedStream(language,function(featured_stream){

    var featured_stream = featured_stream; 
    var stream_id  = featured_stream.id; 

    console.log("the featured stream " + JSON.stringify(featured_stream));

     model.getStreamAds(stream_id,language,function(err,ads){
     
      var ads = ads; 
      var ads_count = Object.keys(ads).length;

      if (ads_count == 0){
  
        // dummy ad so as to not break jade with ad.video reference 
        console.log("no ads");
        ads = [{id:'null',title:'null',video:'null'}];

      }
    
      
    console.log("the featured stream " + JSON.stringify(featured_stream)); 

    // get popular videos

    model.getPopularStreams(language,function(popular_stream_previews){
      var menu = {title:'Popular Streams',streams:popular_stream_previews}
      stream_previews['Popular Streams'] = menu;
      if(sess.user.logged_in){
        var user_id = sess.passport.user.id;

        model.getRecentlyWatched(sess,user_id,language,function(recently_watched){

          console.log("recently watched " + JSON.stringify(recently_watched)); 
          var menu = {title:'Recently Watched',streams:recently_watched}
          stream_previews['Recently Watched'] = menu;
        
           model.getFavourites(sess,user_id,language,function(favourites){
            var menu = {title:'Favourites',streams:favourites}
            stream_previews['Favourites'] = menu;

             console.log("the favourites " + JSON.stringify(favourites));


              console.log("user id " + user_id);
              console.log("stream id" + stream_id);
              model.checkIfFavourited(sess,user_id,stream_id,
                function(is_favourited){
               
             
                model.getVotes(sess,stream_id,user_id,function(user_vote,votes_total){

                  console.log("user vote " + user_vote);
                  console.log("votes total " + votes_total); 

                  var file_type = getFileTypeForHTML(featured_stream.video);      
                   // we just have one ad for now ** 
                  var ad_file_type = getFileTypeForHTML(ads[0].video); 
                  

                res.render('home', { title: 'Home',settings:req.app.locals.settings,
                recently_watched:recently_watched,session:sess,
                favourites:favourites,stream:featured_stream,
                stream_advertisements:ads,
                ad_file_type:ad_file_type,
                popular_streams:popular_stream_previews,
                user_vote:user_vote,votes:votes_total,favourited:is_favourited,
                file_type:file_type,stream_previews:stream_previews} ); 
              });   
          }); 
        });
      });   
    }
   else{


    res.render('home', { title: 'Home',settings:req.app.locals.settings,session:sess,
      stream:featured_stream,stream_advertisements:ads
      ,stream_previews:stream_previews} );   


     }
   })
  }); 
});  
});       			 		            


router.get('/goservices',security.ensureEnabled('go'), function(req,res){
 
  var sess = req.session; 
 

  model.getGoServices(function(err,results){

    console.log("results from route " + JSON.stringify(results));

    res.render('goservices', {title:'goservices',goservices:results,settings:req.app.locals.settings,session:sess})

  })

})   	  
	            
// POSTS 

// insert data related to stream playback 
// insert into customer's recently watched
// incriment video views 
// id is stream id 
router.post('/track_play/:id' , function(req, res) {
// to home page where recently watched and favourites are displayed IF SIGNED ON AS CUSTOMER **
       
 
  var sess = req.session; 

  if(!sess.user.logged_in){
    return res.send("not logged in"); 
  }

  var user_id = sess.passport.user.id; 

  var stream_id = req.params.id; 

  // no account

  if(user_id == null){
    addWatchedToSession(sess,stream_id); 
    return res.send("tracked play")
  }

  else{
    console.log(JSON.stringify(req.app.locals.settings));


    model.streamToWatched(user_id,stream_id,function(){

    return res.send('tracked play');

      });
    }                                                 
 }); 


// called every 5 seconds that a video has been watched for
router.post('/track_playtime/:id/:time' , function(req, res) {
   
  var sess = req.session; 

  if(!sess.passport){
    return res.send("not logged in"); 
  }

  var user_id = sess.passport.user.id; 
  var stream_id = req.params.id; 
  var time = req.params.time; 

  // no account

  if(user_id == null){
    sess.user.watch_queue[stream_id] = time;  
    console.log("HERE " + JSON.stringify(sess)); 
    return res.send("tracked playtime")
  }

  // account enabled 
  // get user queue data from redis cache 
  else{
    
    model.trackPlaytime(req,user_id,stream_id,time,function(){
      console.log("tracked playtime the session " + JSON.stringify(sess));

      return res.send('tracked playtime');
      });
    }                         
                             
});



// **** HELPERS ****** 


function clone(a) {
   return JSON.parse(JSON.stringify(a));
}

function addWatchedToSession(sess,stream_id){
 

    var id = parseInt(stream_id);

    if(!sess.watched){
      sess.watched = [id] 

    }
    else{
      // if not already in favourites
      if(sess.watched.indexOf(id) < 0){
        sess.watched.push(id);
      } 
    }

    console.log("the watched " + sess.watched);
    
}



function serviceEnabled(req,service){

  var enabled_services = req.app.locals.settings['enabled-services'];
    // if service is enabled 
    if (enabled_services.indexOf(service) >= 0){
     return true;
   }
   else{
    return false; 
   }
}

function getCachedCategoriesForType(sess,type){

  if (type == 'livestream'){
    return(sess.livestream_categories); 
  }
  else if (type == 'ondemand'){
    return(sess.ondemand_categories); 
  }
}

// path.mp4 returns video/mp4
// get video type with file extention to be passed to html 
function getFileTypeForHTML(filepath){
  var file_type; 
  var extention = path.extname(filepath); 
  console.log("the extention" + extention);
  if (extention == '.ogv'){
    file_type = 'video/ogg'
  }
  else if (extention == '.mp4'){
    file_type = 'video/mp4'
  }
  else if (extention == '.webm'){
    file_type = 'video/webm'
  }
  // livestreams
  else if (extention.includes('m3u8')){
    file_type = 'application/x-mpegURL'
  }
  else{
    file_type = 'video/' + extention.slice(1,extention.length);
  }
 
  return (file_type); 

}

  
module.exports = router;
