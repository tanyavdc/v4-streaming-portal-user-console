var express = require('express')
  , router = express.Router();

var model = require('../model/user.js');
var passport = require('passport');
var flash = require('connect-flash');

var multer = require('multer');

var security = require('../middleware/security.js');

var sess; 


var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/tmp/')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname)
  }
})



var upload = multer({ storage: storage });


// GETS 

router.get('/account',[security.ensureEnabled('account'),security.ensureAuthenticated],
  function(req,res){

  sess = req.session;

  var account_type = req.app.locals.settings['payment-gateway']; 

  if(account_type == 'stripe'){ 
    var plans = req.app.locals.payment_plans; 
    // get public stripe key to process payment information
    var stripe_p_key = req.app.locals.settings['stripe-p-key']; 
    res.render('account/account-profile-stripe', { title: 'Profile',account:account_type,
      settings:req.app.locals.settings,session:sess,plans:plans,stripe_p_key:stripe_p_key} ); 
  }

  else{
    res.render('account/account-profile', { title: 'Profile',account:account_type,
      settings:req.app.locals.settings,session:sess} ); 
  }
  })

router.get('/login', security.ensureEnabled('account'),  function(req, res) {
	   // to home page where recently watched and favourites are displayed IF SIGNED ON AS CUSTOMER ** 

     // DEPENDING ON ACCOUNT TYPE ** TODO
  
	  sess = req.session;
    var account_type = req.app.locals.settings['payment-gateway']; 
    
    if (account_type == 'account' || account_type == 'saml'){
    res.render('account/login', { title: 'Login', error:req.flash('loginMessage'),
      settings:req.app.locals.settings,session:sess} );	
      }

    else if (account_type == 'stripe'){
      res.render('account/login-stripe', { title: 'Login', error:req.flash('loginMessage'),
      settings:req.app.locals.settings,session:sess} ); 
    }	
        		
     // ip **        	 			    
});

router.get('/signout',security.ensureEnabled('account'),function(req, res) {
     // to home page where recently watched and favourites are displayed IF SIGNED ON AS CUSTOMER ** 
  
    sess = req.session;
    // reset user object 
    sess.user = {logged_in:false}; 
    // passport logout 
    req.logout(); 
    console.log(JSON.stringify(sess));
    res.redirect('back');   
                        
});

router.get('/signup', security.ensureEnabled('account'),  function(req, res) {
     // to home page where recently watched and favourites are displayed IF SIGNED ON AS CUSTOMER ** 

     // DEPENDING ON ACCOUNT TYPE ** TODO
  
  sess = req.session;
  var account_type = req.app.locals.settings['payment-gateway']; 

  
  if (account_type == 'stripe'){
    var plans = req.app.locals.payment_plans; 
    var stripe_p_key = req.app.locals.settings['stripe-p-key']; 


    res.render('account/stripe-signup', { title: 'Signup',
    settings:req.app.locals.settings,session:sess,error:req.flash('loginMessage'),
    plans:plans,stripe_p_key:stripe_p_key} ); 

  }

  else if (account_type == 'account'){

    res.render('account/signup', { title: 'Signup',
    settings:req.app.locals.settings,session:sess,error:req.flash('loginMessage')} ); 

  }                         
});


router.get('/success', function(req,res){
  sess = req.session;


  res.render('home',{msg:"Success",session:sess});


})


// POSTS 

router.post('/login', function(req,res){

  var sess = req.session; 

  passport.authenticate('user-local',function(err, user, info) {

    if (!sess.user.logged_in) {
    // back to login
    res.redirect('back'); 
    }

    // all is well
    else{
      req.logIn(user, function (err) { // <-- Log user in
        // get redirect path 
        if(sess.redirect_to){
         var redirect_path = sess.redirect_to; 
         sess.redirect_to = ''; 
         return res.redirect(redirect_path); 
        }
        // no redirect path specified
        // go home 
        else{
          return res.redirect('/'); 
        }
     });
    }  
  })(req,res);
});

router.post('/login-stripe',function(req,res){

  var sess = req.session; 

  passport.authenticate('stripe-local',function(err, user, info) {

     
    if(sess.user.plan_expired){
      res.redirect('/signup')
    }

    else if (!sess.user.logged_in) {
    // back to login
    res.redirect('back'); 

    }

    // all is well
    else{
      req.logIn(user, function (err) { // <-- Log user in
        // get redirect path 
        if(sess.redirect_to){
          var redirect_path = sess.redirect_to;
          sess.redirect_to = ''; 
         return res.redirect(redirect_path); 
        } 
        // no redirect path specified
        // go home 
        else{
          return res.redirect('/'); 
        }
      });
    } 
  })(req,res);
});

router.post('/stripe-signup', security.ensureEnabled('account'),  function(req, res) {
     // to home page where recently watched and favourites are displayed IF SIGNED ON AS CUSTOMER ** 

     // DEPENDING ON ACCOUNT TYPE ** TODO
  
  sess = req.session;

  var token = req.body['stripe-token'];
  var email = req.body['email'];
  var subscription = req.body['subscription-plan']; 



   model.stripeSignup(token,email,subscription,function(err){

    //if err
    // render err 
   
    if(!err){
      
      passport.authenticate('stripe-local')(req, res, function () {

        res.render('success', { title: 'Sign Up', msg:"Processed payment successfully",
        settings:req.app.locals.settings, session:sess} );   
      });
    }
  })                        
});

router.post('/signup', security.ensureEnabled('account'),  function(req, res) {
     // to home page where recently watched and favourites are displayed IF SIGNED ON AS CUSTOMER ** 

     // DEPENDING ON ACCOUNT TYPE ** TODO
  
  sess = req.session;

  // hash password ** 
  var raw_password = req.body['password']; 
  var hash = bcrypt.hashSync(req.body['password'], salt);
  console.log("the hash " + hash); 
  var email = req.body['email'];

   model.signup(email,hash,function(err){

    //if err
    // render err 

    if(!err){
      // log user in 
      passport.authenticate('user-local')(req, res, function () {

        res.render('success', { title: 'Sign Up', msg:"Signed up successfully",
        settings:req.app.locals.settings, session:sess} );  
      }); 
    }

  })                        
});

router.post('/edit-profile',[security.ensureEnabled('account'),
  security.ensureAuthenticated],function(req,res){

  var sess = req.session; 
  var user_id = sess.passport.user.id;
  var new_password_hash = bcrypt.hashSync(req.body['new-password'], salt);
  var old_password_hash = bcrypt.hashSync(req.body['old-password'], salt);
  var new_email = req.body['email']; 

  var edited_password = req.body['edited-password']

  if(edited_password){
    model.validateUser(user_id,old_password_hash,function(passed){

    // old password matches password for this user
    if(passed){

      // proceed to edit account details
      model.editAccount(user_id,new_password_hash,req,function(err){

        console.log("the sess " + JSON.stringify(sess)); 
        res.render('success', { title: 'Sign Up', msg:"Updated details successfully",
        settings:req.app.locals.settings, session:sess} );  
      });
    }

    else{
      req.flash('editMessage',"Wrong password")
      res.render('account/account-profile', { title: 'Profile',account:'account',
      settings:req.app.locals.settings,session:sess,error:req.flash('editMessage')} ); 
      }
    });
  }

  // just edited email 
  else{
    model.editEmail(user_id,new_email,sess,function(err){
      res.render('success', { title: 'Sign Up', msg:"Updated details successfully",
      settings:req.app.locals.settings, session:sess} );  
    });
  }

  
  })


router.post('/edit-payment-profile',[security.ensureEnabled('account'),
  security.ensureAuthenticated],function(req,res){
  sess = req.session;

  var user_id = sess.passport.user.id;
  var old_email = sess.passport.user.email; 

  var new_email = req.body['email'];  // = old email if user did not edit email 
 
  var account_type = req.app.locals.settings['payment-gateway']; 


  var stripe_id = sess.user.stripe_id;
  var subscription_id = sess.user.subscription_id;  

  // edit stripe email and/or credit card 
  model.editPaymentProfile(user_id,stripe_id,req,function(edited){

    model.changePaymentPlan(user_id,subscription_id,req,function(edited){

       // save edited_user to db 
       model.editEmail(user_id,new_email,sess,function(){
        console.log("the sess " + JSON.stringify(sess)); 
        res.render('success', { title: 'Sign Up', msg:"Updated details successfully",
        settings:req.app.locals.settings, session:sess} );  
      })
    })
  });
});

    
router.post('/translate/:language', function(req,res){
 
  var sess = req.session; 
  sess.current_language = req.params.language;

  console.log("the session " + JSON.stringify(sess)); 
  res.send("translated"); 
})


// favouriting a video
// called by ajax when favourite button is clicked 
router.post('/favourite' , security.ensureAuthenticated, function(req, res) {
       // to home page where recently watched and favourites are displayed IF SIGNED ON AS CUSTOMER **
       
 
  var sess = req.session; 
  var user_id = sess.passport.user.id; 

  var stream_id = req.body['stream-id'];

   // no account 
  if(user_id == null){
    addFavouriteToSession(sess,stream_id);
    return res.send('favourited'); 
  } 
 
  model.favouriteStream(user_id,stream_id,function(err){

    if(err){
      return res.status(500).send("Stream DNE")
    }

    else{
     return res.send('favourited');
    }

  });   
            
}); 


// called by ajax when unfavourite button is clicked 
router.post('/unfavourite' , security.ensureAuthenticated, function(req, res) {
       // to home page where recently watched and favourites are displayed IF SIGNED ON AS CUSTOMER **
       
 
  var sess = req.session; 
  var user_id = sess.passport.user.id; 

  var stream_id = req.body['stream-id'];

  // no account 
  if(user_id == null){
    removeFavouriteFromSession(sess,stream_id);
    return res.send('unfavourited'); 
  } 

  // account

  else{

    model.unfavouriteStream(user_id,stream_id,function(){
    
       return res.send(); 

      });
    }
                      
 }); 

    
router.post('/upvote/:stream_id',security.ensureAuthenticated, function(req,res){
 
  var sess = req.session; 

  var stream_id = req.params.stream_id; 

  var user = sess.passport.user['id']; 


  // user accounts are disabled so save vote to this session 
  // when to expire ?? ** 
  if (user == null){
    saveVoteToSession(sess,1,stream_id);
    return res.send(); 

  }

  // if user's have accounts
  // save to db for this user
  else{
    // get user 
    model.vote(1,stream_id,user,function(err){

      if (err){
        res.status(500).send("Stream DNE")
      }

      else{
    
       return res.send(); 
      } 
    }); 
  }



})

router.post('/unvote/:stream_id',security.ensureAuthenticated, function(req,res){
 
   var sess = req.session; 
   var stream_id = req.params.stream_id; 

   var user = sess.passport.user['id']; 


  // user accounts are disabled so save vote to this session 
  // when to expire ?? ** 
  if (user == null){
    saveVoteToSession(sess,-1,stream_id);
    return res.send(); 

  }

  // if user's have accounts
  // save to db for this user
  else{


    // get user 
    
  
    model.vote(-1,stream_id,user,function(err){
    
      if (err){
        res.status(500).send("Stream DNE")
      }

      else{
    
       return res.send(); 
      } 
      
    }); 
  }

})


    
router.post('/downvote/:stream_id',security.ensureAuthenticated, function(req,res){
 
   var sess = req.session; 
   var stream_id = req.params.stream_id; 

  // if user's have accounts
  // save to db for this user
  if (serviceEnabled(req,'account')){
    // get user 
    var user = sess.passport.user['id']; 

    model.vote(-1,stream_id,user,function(err){
      // if err, user has already voted    
      if (err){
        res.status(500).send("Stream DNE")
      }

      else{
    
       return res.send(); 
      } 
      
    }); 
  }

  // to session 
  else {  
      saveVoteToSession(sess,-1,stream_id); 
      console.log(JSON.stringify(sess));
      res.send();    
    }
    

})


// **** HELPERS ****** 


function serviceEnabled(req,service){

  var enabled_services = req.app.locals.settings['enabled-services'];
    // if service is enabled 
    if (enabled_services.indexOf(service) >= 0){
     return true;
   }
   else{
    return false; 
   }
}


// vote = 1 for upvote, -1 for downvote
function saveVoteToSession(sess,vote,stream_id){

  sess.votes[stream_id] = vote; 
    
}



function removeFavouriteFromSession(sess,stream_id){
  console.log("unfavouriting stream in model");

  var id = parseInt(stream_id); 

  

  removeFromArray(sess.favourites,id); 

  
  console.log("the favourites " + sess.favourites);
    

}

function addFavouriteToSession(sess,stream_id){
  console.log("unfavouriting stream in model");

    var id = parseInt(stream_id);

    if(!sess.favourites){
      sess.favourites = [id] 

    }
    else{
      // if not already in favourites
      if(sess.favourites.indexOf(id) < 0){
        sess.favourites.push(id);
      } 
    }

    console.log("the favourites " + sess.favourites);
    


}

function removeFromArray(array,item){

  var index = array.indexOf(item);

  if (index > -1) {
   array.splice(index, 1);
  }
  return array; 
}


       
module.exports = router;
