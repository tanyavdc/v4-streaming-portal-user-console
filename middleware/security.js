exports.ensureAuthenticated = function(req, res, next) {

  console.log("in security middleware getting form " 
    + req.body['stream-id'])

  var sess = req.session;
  var auth_type = req.app.locals.settings['payment-gateway'];
  var account_enabled = req.app.locals.settings['enabled-services'].indexOf('account') 
      < 0 ? false : true

  if (account_enabled){

	  if (req.isAuthenticated()){
		    return next();
		  }
		  else{
      req.flash('loginMessage', 'You must be logged in to access this content');
      // save request url to redirct to after successful login
      sess.redirect_to = req.path;
      console.log("the redirect path " + req.path); 
      res.redirect('/login');
		//	res.render('login',{error:'You must be logged in to access this feature',session:sess});
		    // Return error content: res.jsonp(...) or redirect: res.redirect('/login')
		}
  }

  
  else if(auth_type == 'ip'){

    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("the ip " + ip);

    if(ip == '232323'){
      return next(); 
    }

    else{
      res.render('home',{error:'You must be located in the USSR to view this content',session:sess});
    }
  }

  else if(auth_type == 'none'){

    next(); 


   


    // dummy user
   // sess.passport = {user:{id:null}}; 
    
    

    }

	};
  


// INCOMPLETE ** 
exports.ensureEnabled = function(service) {


  return function(req,res,next){
    var sess = req.session;
    var enabled_services = req.app.locals.settings['enabled-services'];
    // if service is enabled 
    if (enabled_services.indexOf(service) >= 0){
     return next(); 
   }
    else{
      res.render('home',{error:'Page is not available',session:sess});

    }
  }
};


