// setup app local variables by fetching site specific values from redis cache 


var cache= require('../model/cache.js');


var stripe = require("stripe")();


var sess; 


// user specific settings to sess (current language, translated content)
// domain specific settings to app locals (enabled langs, default lang, 
// colors, account type, etc)
module.exports = function(req, res, next) {

	sess = req.session;
  
  var current_language; 

  if(!sess.user){
    sess.user = {logged_in:false}; 
  }

  // instantiate sessions

  // user object holds non login session data for user, like continue watching from times
  
  // fetch user's watch time data from redis 

	if(req.isAuthenticated()){
    sess.user.logged_in  = true; 
		var user_tag = "user:" + sess.passport.user.id
    // if not exist, set **TODO 

   
    cache.exists(user_tag,function(err,reply){
       // user exists in cache 
      if(reply === 1){
        cache.hgetall(user_tag,function(err,reply){
          sess.user.watch_queue = reply; 
        })

      }

      // user DNE in user to watched queue cache
      // cache this user 
      else{ 

         cache.hset(user_tag,'null','null',function(err,reply){
         
          sess.user.watch_queue = {}
        });
      }
    });
  }


  if(!sess.current_language){
  	sess.current_language = req.app.locals.default_language;
    
  }

	// get categories 		



 var livestream_categories_tag =  "livestream-categories-" + sess.current_language; 
  cache.lrange(livestream_categories_tag,0,-1, function(err,reply) {

        sess.livestream_categories = listToMap(reply);
  			 
				
			 });

	// get ondemand categories 
			
	var ondemand_categories_tag = "ondemand-categories-" + sess.current_language; 
			 cache.lrange(ondemand_categories_tag,0,-1, function(err,reply) {
				 sess.ondemand_categories = listToMap(reply);
				
			 });

  // get enabled languages     
	
	cache.lrange('enabled-languages',0,-1,function(err,reply){
		req.app.locals.enabled_languages = reply; 
	});

  // get admin-set domain settings 

  cache.hgetall('domain',function(err,reply){

    req.app.locals.settings = reply;  

    // save language content to user session according to their current lang
    var tag = 'homepage-msg-' + sess.current_language;   
    sess.homepage_msg = req.app.locals.settings[tag];


    req.app.locals.default_language = reply['default-language'];


    cache.lrange("enabled-services",0,-1, function(err,reply) {
  		
  		req.app.locals.settings['enabled-services'] = reply; 
     

      var account_type = req.app.locals.settings['payment-gateway'];

      setupAccount(req,account_type,function(){
        next(); 
      });
  	});
  });
};

// ** HELPERS **

// listToMap([2:a,3:b,somekey:somevalue]) => {2:a,3:b,somekey:somevalue}
function listToMap(list){
  var map = {}; 
  for (var i = 0 ; i < list.length ; i ++){
    var pair = list[i]; 
    var key = pair.split(":")[0];
    var value = pair.split(":")[1];
    map[key]=value; 

  }
 return map; 

}

// ** ASYNC?? 
function setupAccount(req,account_type,callback){
  var sess= req.session; 


  if(account_type == 'none'){
    // setup dummy user
   
    sess.passport = {};
    sess.passport.user = {};
    sess.passport.user['id'] = null;
    sess.user.logged_in = true; 
    return callback(); 
  }

  else if(account_type == 'stripe'){

    var stripe_s_key = req.app.locals.settings['stripe-s-key'];

    var plans_list = []; 

    stripe.plans.list(
    { api_key:
    stripe_s_key },
    function(err, plans) {
      if (err || !plans){
        return callback(); 
      }

      for (var i = 0; i < plans.data.length ; i ++){
        var plan = plans.data[i];
        var cost = plan.amount/100 ; // ammount $10 stored in stripe as 1000
        var plan_obj =  {id:plan.id,cost:cost,interval:plan.interval,name:plan.name}
        plans_list.push(plan_obj);
      
      }
      req.app.locals.payment_plans = plans_list; 
     
    });
    return callback();
  }

}