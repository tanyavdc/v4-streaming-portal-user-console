// setup app local variables by fetching site specific values from redis cache 


var cache= require('../model/cache.js');

var sess; 

// user specific settings to sess (current language, translated content)
// domain specific settings to app locals (enabled langs, default lang, 
// colors, account type, etc)
module.exports = function(req, res, next) {


  sess = req.session; 
  

  // if current language not set, set to default lang

  if(!sess.current_language){
    sess.current_language = req.app.locals.default_language;
  }
  
  // get account type 

 cache.lrange("enabled-services",0,-1, function(err,reply) {


  
  req.app.locals.settings['enabled-services'] = reply; 
  console.log(req.app.locals.settings['enabled-services'])

  var account_required = reply.indexOf('account') > 0  ? true : false; 



  // instantiate session for no account user
  if(!account_required){

    console.log("creating session account");

    // user session is not instantiated 
    if(!sess.session_id){
      // create user session 'account'

      registerUserSession(sess,function(session_id){
       
        getUserWatchQueue('sess',session_id,function(queue){
          sess.user_watch_queue = queue; 

          next()}); 
      });
    }

    // user session is set
    else{
      getUserWatchQueue('sess',sess.session_id,function(queue){
          sess.user_watch_queue = queue; 
          next(); 
        });
      }
  }

  // account required 
  else{
    if(req.isAuthenticated()){
      getUserWatchQueue('user',sess.passport.user.id,function(queue){
        sess.user_watch_queue = queue; 
        next();
        }); 
      }
      next(); 
    }
}); 
}

// for domains with account disable 
// create a session 'account' for the user 
function registerUserSession(sess,callback){
  

  // create a dummy passport user  
  // so that user will be considered authenticated 

  sess.passport = {};
  sess.passport.user = {}; 
  sess.passport.user['id'] = null;

   // instantiate session user data objects 
  sess.votes = {}; 
  sess.favourites = {}; 

  sess.logged_in = true;    



  // generate a session id 
   cache.exists('session-user-count',function(err,reply){
    
    if(reply === 1){
      
      cache.incr('session-user-count',function(err,reply){
      
        sess.session_id=reply; 
        callback(reply); 

      })
    }
    else {
      cache.set('session-user-count',1,function(err,reply){
        sess.session_id=1; 
        callback(1); 

      })

    }
  }); 

}

function getUserWatchQueue(account_type,id,callback){

  // get user cache data from redis (stream queue times)

  // set current language 

  var tag = account_type + ":" + id; 
  // if not exist, set **TODO 


  cache.exists(tag,function(err,reply){
   // user exists in cache 
  if(reply === 1){
    cache.hgetall(tag,function(err,reply){


      callback(reply); 
    })

  }

  else{ 
    // instantiate
    cache.hset(tag,'null','null',function(err,reply){
      callback({null:null});
     });
    }
  });
}  
