
/**
 * Module dependencies.
 */

var express = require('express')
    , http = require('http')
    , logger = require('morgan')
    , flash = require('connect-flash')
    , path = require('path');

config = require('./config');
//config = require('./config.json')[app.get('env')];

bcrypt = require('bcryptjs');
salt = bcrypt.genSaltSync(10);


const DEFAULT_SECONDARY_COLOR = '#1c75bc';
const DEFAULT_PRIMARY_COLOR = '#ffffff'


var cookieParser = require('cookie-parser');
var redis_client = require("./model/cache.js");


var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
require('./model/account')(passport);
var security = require('./middleware/security.js');

var favicon = require('serve-favicon');
var methodOverride = require('method-override');

var session = require('express-session');
var RedisStore = require('connect-redis')(session);


var bodyParser = require('body-parser');
var multer = require('multer');
var errorHandler = require('errorhandler');

var router = express.Router();

var app = express();



// set site specific variables that entire app can access 

app.locals.enabled_languages;
app.locals.livestream_categories;
app.locals.ondemand_categories;
app.locals.advertisement_categories;
app.locals.settings={primary_colour:DEFAULT_PRIMARY_COLOR,secondary_colour:DEFAULT_SECONDARY_COLOR};

app.locals.default_language = 'en'; 
// TO USER SESS 
app.locals.current_language;  



app.use(bodyParser.json());



require('./model/account')(passport); 


app.use(cookieParser());

app.use(session({
    secret: 'kittykat2',
    name: 'client.sid',
    resave: false,
    saveUninitialized: true,
    store: new RedisStore({client:redis_client})
}));


// setup passport for authentication 
app.use( passport.initialize());
app.use( passport.session());
app.use(flash());

app.use(function(req, res, next){
    res.locals.success_messages = req.flash('success_messages');
    res.locals.error_messages = req.flash('error_messages');
    next();
});

// setup redis cache 
var cache_setup = require("./middleware/cache-setup");
// sets up site specific local variables  
app.use(cache_setup);




// logger 
var myLogger = function (req, res, next) {
	  console.log('LOGGED');
	  console.log('middleware recieved this response ' + res);
	  console.log('middleware recieved this request' + req);
	  next();
	};

//request converter   **** 

var converter = function (req, res, next) {
	// console.log('converting response');
	 var converted = "CONVERTED"; 
	 var msg_from_router = req;
	 console.log(req);
     next();
	}; 


  

app.set('port', config.web.port);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(methodOverride());

// parse application/json
app.use(bodyParser.json());                        

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

// parse multipart/form-data


app.use(express.static(path.join(__dirname, 'public')));




 

var routes = require('./routes')
   , user = require('./routes/user')
   , streams = require('./routes/streams')
    , index = require('./routes/index')



// hook up routers
app.use(router);  
app.use(streams);
app.use(user);

// custom 404 redirect
app.use(function(req, res, next){
  res.status(404);

  // respond with html page
  if (req.accepts('html')) {
    res.render('404', { session:req.session, msg:"Sorry" });
    return;
  }

  // respond with json
  if (req.accepts('json')) {
    res.send({ error: 'Not found' });
    return;
  }

  // default to plain-text. send()
  res.type('txt').send('Not found');
});


// MIDDLEWARE *** 




/*  
app.get('/success', security.ensureAuthenticated, function(req, res) {
   // to model to get livestreams of category 
	console.log(req.flash("loginMessage"));
	
 res.send("success");
	
	            			 
	           
		});  

  */   	  

app.get('/fail',function(req, res) {
	   // to model to get livestreams of category 
	

	console.log(req.flash("loginMessage"));
	res.send('error');
	 
			});    



   
// development only
if ('development' == app.get('env')) {
  app.use(errorHandler());
}




// routers 
router.use(function(req, res, next) {

    // log each request to the console
    console.log(req.method, req.url);

    // continue doing what we were doing and go to the route
    next(); 
});


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

