// FORM VALIDATATORS

// stripe signup 

function stripeResponseHandler(status, response) {
  var $form = $(".payment-form");
  if (response.error) {
    $('.error')
      .removeClass('hide')
      .find('.alert')
      .text(response.error.message);
  } else {
    // token contains id, last4, and card type
    var token = response['id'];
    // insert the token into the form so it gets submitted to the server
    // clear sensitive input data from form
    $('.sensitive').empty();
    $form.append("<input type='hidden' name='stripe-token' value='" + token + "'/>");
    $form.get(0).submit();
  }
}