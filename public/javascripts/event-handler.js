$(document).ready(function(){


$(document.body).on('click', '.change-lang',function(){

    var lang = $(this).attr('id');
    console.log("translating to " + lang);

    $.ajax({ 
      type: "POST",
      url: '/translate/' + lang ,
      // SEND TITLES IN RES 
      success: function (res) {
        
        
        console.log("translated");
        // toggle button 
         
        location.reload(); 

      },
      error: function(xmlhttp, status, error){ 
        console.log("error " + error);

             
      }
    })
  }) 

// STREAM LAYOUT EVENTS 

$( ".stream-box" ).click(function() {
        
       /* // reset all other controls
        $('.stream-description').show();
        $('.controls').hide();  */ 
        // show this streams controls 
        $(this).find('.stream-description').toggle();
        $(this).find('.controls').toggle();

      });


$(".remove").click(function(){
  var confirmation = confirm("Remove stream?");
  if (confirmation == true) {
     var stream_id = this.id.split('-')[1];
     var form = $(this).parents('form:first');
     form.attr('action', '/remove_stream/'+ stream_id);
     form.attr('method','post');
     form.submit();      
  }
  });

$(".advanced-settings-btn").click(function(){
  $(".advanced-settings").toggle(); 
  var text = $(".advanced-settings-btn").text();
  console.log(text); 
  if (text == "Show Advanced Settings"){
    $(".advanced-settings-btn").text("Hide Advanced Settings");
  }
  else{
    $(".advanced-settings-btn").text("Show Advanced Settings");
  }


})

// SIGNUP AND LOGINS

// stripe signup
// subscription type toggles
$(document.body).on("click", "input[name='subscription-plan']",function(){
  if (this.value == 'daily'){
     $("#amount").text(' $2');

  }
  else if (this.value == 'weekly'){
    $("#amount").text(' $10');

  }

  else if (this.value == 'monthly'){
     $("#amount").text(' $30');
  }
});

// submit stripe signup form 
$('#payment-submit').click(function(){

  var $form = $(".payment-form");

  if (!$form.data('cc-on-file')) {
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }


});

// edit account

// normal account edit form 
$('#edit-form-submit').click(function(){


  var $form = $(".account-form");

  // call validator **

  // if no errors
  // submit form  
  $form.get(0).submit(); 
});

// stripe account edit form 
$('#edit-payment-form-submit').click(function(){


  var $form = $(".payment-form");


  if($("input[name='edited-creditcard']").val() == 'true'){
    if (!$form.data('cc-on-file')) {
        Stripe.setPublishableKey($form.data('stripe-publishable-key'));
        Stripe.createToken({
          number: $('.card-number').val(),
          cvc: $('.card-cvc').val(),
          exp_month: $('.card-expiry-month').val(),
          exp_year: $('.card-expiry-year').val()
        }, stripeResponseHandler);
      }
  }

  // credit card info hasn't been edited
  // submit form without creating token
  else{
    $form.get(0).submit(); 
  }
});


$('#email-input').focusout(function(){
  $(this).addClass('hidden'); 
  var edited_email = $('#email-input').val();
  $('#email').text(edited_email);
  $('#email').removeClass('hidden');
  $('#email-edit-btn').removeClass('hidden');  
})
  


// STREAM PAGE EVENTS 

$(document.body).on('click', '.upvote',function(){

  console.log("clicked");
    

   var stream_id = this.id.split('-')[1];
   var button = $(this); 
   var vote_count = $('#vote-count').text();  
 
   $.ajax({ 
      type: "POST",
      url: '/upvote/' + stream_id ,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      data: $(this).closest('form').serialize(),

          
          // SEND TITLES IN RES 
          success: function (res) {    
           
            console.log("upvoted");
              // toggle buttons
             
             // negate previous downvote 
             
              $('.downvote').removeClass("voted"); 
              button.addClass("voted"); 
              button.removeClass("upvote")
              

              // incriment vote count 
              vote_count ++; 
              $('#vote-count').text(vote_count); 
              
          },

          error: function(xmlhttp, status, error){     
        
            console.log("the error " + xmlhttp.responseText);

            var error_msg = xmlhttp.responseText;
            console.log(error_msg); 
            alert(error_msg); 
          }
      })
  });

$(document.body).on('click', '.downvote',function(){
   var stream_id = this.id.split('-')[1];
   var button = $(this);
   var vote_count = $('#vote-count').text();  

 
 
   $.ajax({ 
      type: "POST",
      url: '/downvote/' + stream_id ,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      data: $(this).closest('form').serialize(),

          
          // SEND TITLES IN RES 
          success: function (res) {
          
           // negate previous upvote 
           
            $('.upvote').removeClass("voted"); 
            button.addClass("voted"); 
            button.removeClass("downvote")
            
            
            
            // decriment vote count
            vote_count = vote_count - 1;

            $('#vote-count').text(vote_count); 
      
          },

          error: function(xmlhttp, status, error){ 
            console.log("the error " + xmlhttp.responseText);

            var error_msg = xmlhttp.responseText;
            console.log(error_msg); 
            alert(error_msg); 
            
          }
        })
      });


// unvote 
$(document.body).on('click', '.voted',function(){
   var stream_id = this.id.split('-')[1];
   var button = $(this);
   var vote_count = parseInt($('#vote-count').text());  

 
 
   $.ajax({ 
      type: "POST",
      url: '/unvote/' + stream_id ,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      data: $(this).closest('form').serialize(),

          
          // SEND TITLES IN RES 
          success: function (res) {

            // upvote or downvote 
            // raw id : upvote-[stream_id]
            var vote_tag = button.attr('id').split('-')[0]; 


            button.removeClass('voted'); 
            // vote tag is either upvote or downvote
            button.addClass(vote_tag); 
        
            
            // undo upvote
            if (vote_tag == 'upvote'){
                vote_count = vote_count - 1;
            }
            // undo downvote
            else {
              vote_count = vote_count + 1;
            }
          

            $('#vote-count').text(vote_count); 
      
          },

          error: function(xmlhttp, status, error){ 
            // user has already upvoted


            var error_msg = xmlhttp.responseText;

            handleVoteError(error_msg,button);

            
          }
        })
      });



$(document.body).on('click', '.favourite',function(){

  var button = $(this); 
 
   $.ajax({ 
      type: "POST",
      url: '/favourite' ,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      data: $(this).closest('form').serialize(),

          
          // SEND TITLES IN RES 
          success: function (res) {
            
            
            console.log("favourited");


            // toggle button 
            button.removeClass("favourite");
            button.text("Unfavourite"); 
            button.addClass("unfavourite"); 
                
          },
          error: function(xmlhttp, status, error){ 

            console.log("the error " + xmlhttp.responseText);

            var error_msg = xmlhttp.responseText;
            console.log(error_msg); 
            alert(error_msg); 
        }
      })
    });

$(document.body).on('click', '.unfavourite',function(){

  var button = $(this); 
 
   $.ajax({ 
      type: "POST",
      url: '/unfavourite' ,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      data: $(this).closest('form').serialize(),

          
          // SEND TITLES IN RES 
          success: function (res) {
            
            console.log("unfavourited");

            // toggle button 
            button.removeClass("unfavourite");
            button.text("Favourite"); 
            button.addClass("favourite"); 
                      
          },
          error: function(xmlhttp, status, error){ 
            console.log("the error " + xmlhttp.responseText);

            var error_msg = xmlhttp.responseText;
            console.log(error_msg); 
            alert(error_msg); 
          }
        })
      });


// ACCOUNT FORM 

// show the field associated with this edit button, hide the span 
// i.e #email-edit is clicked -> input with name=email is shown 
// span named email is hidden
$('.edit-btn').click(function(){
  var field = this.id.split('-')[0];


  
  var input_tag = "#" + field + "-input"; 
  var tag = "#" + field;  
  
  $(tag).addClass('hidden');
  $(input_tag).removeClass('hidden'); 
  $(this).addClass('hidden');


  if(field == 'credit'){

    // tell form that credit info has changed 
   
    $("input[name='edited-creditcard']").val(true);
  }

  if(field == 'plan'){

    // tell form that plan has changed 
     $("input[name='edited-plan']").val(true);

  }

  if(field == 'password'){

    // tell form that plan has changed 
     $("input[name='edited-password']").val(true);

  }
});


// SETTINGS FORM

  var new_langs = ''; 
  $('#upload-logo').hide();
  $('#new-lang-container').children().hide();

  $('#change-logo').click(function(){
    $('#upload-logo').show();
  })

  // add language 
  $('#add-lang').click(function(){
    $(this).hide();
    $('#new-lang-container').children().show();
    
  })

  
  $('#confirm-lang').click(function(){
    $('#new-lang-container').children().hide();
    $('#add-lang').show();
    var new_lang = $('#new-lang').val();
    console.log("new_lang is " + new_lang);
    // add it to radio buttons 
    //  NEW LANG CONTROL ** MAP TO A TAG  I.E ENGLISH -> EN  ** TODO
    var element = '<label class="radio-inline"><input type="radio" name="lang" value="' + new_lang +'"/>' + new_lang + 
    '</label>';
    
    // add homepage input box for this language 
    addFieldsForNewLanguage(new_lang); 
    
    // add new lang to hidden input for new langs 
    var new_langs = $('#new-langs').val();
    var s =  new_lang + " ";
    var new_langs = new_langs + s;
    console.log("new langs after concat " + new_langs);

    // add to enabled langs hidden input ** TODO 

    appendToElementListValue(new_lang, '#enabled-langs');

    $('#new-langs').val(new_langs);
  
    $('#langs-container').append(element);
   
    $('#added-lang').val('true');
    


  });


  // dynamic elements 

  // sidebar 

   $(document.body).on({
  mouseenter: function() {
     $(this).find('.category-control').show();
  },
  mouseleave: function() {
     $(this).find('.category-control').hide();
  }
}, '.category');



$(document.body).on('click', '.category-control',function(){
  var category_to_remove = $(this).attr('id').split('-')[2];
  var stream_type = $(this).attr('id').split('-')[1];



  var confirmation = confirm("Remove category " + category_to_remove + "?");
  if (confirmation == true) { 
    $.ajax({ 
      type: "POST",
      url: '/remove_category/' + category_to_remove,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      data: $(this).closest('form').serialize(),

          
          // SEND TITLES IN RES 
          success: function (res) {
            

          
            var redirect_link = 'http://localhost:8093/stream_by_type/' + stream_type;
         window.location.replace(redirect_link);
      
          },
          error: function(xmlhttp, status, error){ 
            console.log("error " + error);
            
          }

        });

      };

    });
  });


//*** HELPERS ***

// append item_to_append to element_tag value 
function appendToElementListValue(item_to_append,element_tag){

    var input_value = $(element_tag).val();
    input_value_list = input_value.split(',');
    input_value_list.push(item_to_append);
    $(element_tag).val(input_value_list); 

}






