function serviceIsEnabled(enabled_services,service){

  if(enabled_services.indexOf(service) < 0){
    console.log("service " + service + " is unavailable")

    return false;
  }

  else{
    return true; 
  }
}

// set colours 
function paint(primary_colour,secondary_colour){
  var old_colour; 
  $(".primary-colour-bg-element").css("background",primary_colour);
    $(".secondary-color-element").css("color",secondary_colour);
  
    $(".secondary-color-hover").mouseover(function() {

      old_colour=$(this).css("color");
      console.log("the old colour " + old_colour);

      
      $(this).css("color",secondary_colour);
        }).mouseleave(function() {
      $(this).css("color",old_colour);
      });
}



function fileTypeToTypeTag(){
      return 'application/x-mpegURL'
}



function setupPlayer(settings){

  console.log(JSON.stringify(settings)); 

  // time that video is at 
  var at;

  // record whether or not video (not ad) was played
  var played = false; 


  videojs(
  'the_video',
    {
      autoplay: settings.featured || settings.type == 'livestream',
     }
,
    function() {

      var player = this;
      var ads = []; 

      if(settings.has_ads){
        ads[1] = {
            "src": settings.ad,
            "type": settings.video_type,
          };

         player.on('readyforpreroll', function() {
          player.ads.startLinearAdMode();
          // play your linear ad content
          player.src(settings.ad);
         // when all your linear ads have finished… do not confuse this with `ended`
          player.one('adended', function() {
            player.ads.endLinearAdMode();
          });
        });
      
        player.on("adstart", function() {
          console.log('ads start');
          $('#video-wrapper').append('<div class="ad-playing">Advertisement</div>');
        });
      }
      

      player.one('contentplayback', function() { 
        played = true; 

        if(settings.type != 'livestream' && !settings.featured){
          
         
          // set player to play from time user left off at
          if(settings.user_queue != '' && settings.user_queue != undefined){
           player.currentTime(settings.user_queue);
          }
        }

        // track user play for this stream 

         $.ajax({ 
            type: "POST",
            url: '/track_play/' + settings.stream_id,
                
                // SEND TITLES IN RES 
                success: function (res) {
                  
                  
                  console.log("Tracked play"); 
                
            },
                  error: function(xmlhttp, status, error){ 
                    console.log("error " + error);
                    
                    }
          });

        
        console.log("playing video"); 
      
      });

     

      player.on("contentplayback", function () {
         console.log("playing video"); 

        watchedTime = setInterval(function(){


        // Periodically save current timespot
        at = player.currentTime();
    
         
         
        }, 1000);  // every second

        if(settings.type == 'livestream') {
          $('#the_video').addClass('vjs-live');
          $('.vjs-live-control').removeClass('vjs-hidden');
        }
       

      });

    
      player.on("contentended", function(){
        console.log("content ended");

  
    });

    
    player.adsIntegration({ adSources: ads
        });


      
    } 
  );  

  videojs.options.flash.swf = "/fonts/video-js.swf"  

  // on page exit
  // save and record timespot on video 
  window.onbeforeunload = function (e) {

    if(played){
      $.ajax({ 
          type: "POST",
          url: '/track_playtime/' + settings.stream_id + '/' + at,
              
              // SEND TITLES IN RES 
              success: function (res) {
                
                
                console.log("Tracked playtime"); 
              
          },
                error: function(xmlhttp, status, error){ 
                  console.log("error " + error);
                  
                  }
        });
    }

  };




}
