var pg = require('pg');
var db_client = require('./database');
var cache = require('./cache');

/*server declaration
...
...
*/

require('jquery');

// GETS 

exports.getGoServices=function(callback){

	var query = db_client.query("SELECT id,title,thumbnail,link FROM goservice");

	query.on("row", function (row, result) {
	    result.addRow(row);
		    
		});
			 
		query.on("end", function (result) {

			console.log("results from model " + JSON.stringify(result))
				 
		 //db_client.end();
	     return callback(undefined,result.rows);
			       
		}); 
		
}

// user_id is null if account is disabled 
exports.getVotes = function(sess,stream_id,user_id,callback){

  // 0 for no vote, 1 for upvote, -1 for downvote
  var user_vote = 0;
  var vote_total = 0; 

  // get vote total 
   var query = db_client.query("SELECT count(vote) from votes");  


  query.on("row", function (row) {
      vote_total = row.count; 
        
    });
       

  query.on("end", function (result) {

    // if no user, get their vote from session
    if (user_id == null){
      user_vote = getSessionVote(sess,stream_id); 
      console.log("user vote from sess " + user_vote);
      return callback(user_vote,vote_total);
    }
    // get users vote for stream

    var query = db_client.query("SELECT vote FROM votes WHERE stream_id=$1 AND"
      + " user_id=$2",[stream_id,user_id]);



    query.on("row", function (row) {
        user_vote = row.vote; 
          
      });
         
    query.on("end", function (result) {

     return callback(user_vote,vote_total);
           
      });
  }); 
}

exports.getFeaturedStream = function(language,callback){

  var stream; 

  // get featured stream id 
  var query = db_client.query('SELECT s.id,s.type from stream s' +
  ' WHERE s.featured=true order by s.pub_start desc LIMIT 1'); 

  // get other details 

  // VIDEO FIELD TO STREAM TABLE **TODO 


  query.on("row", function (row) {
    
    stream = row;

  });

  query.on("end", function () {
    if (stream != null){

      getStream(stream.id,language,stream.type,function(err,stream){
        return callback(stream); 
      })
     } 

    // no featured streams ***TODO
    else{
      callback(null); 
    } 
  });
};  


exports.streamToWatched = function(user_id,stream_id,callback){
  console.log("called stream2watched");

	// add new category to database 

  // check if user has watched this video 
  var query = db_client.query("SELECT * FROM user2watched WHERE user_id=$1 AND stream_id=$2",[user_id,stream_id]);

  query.on("row",function(row,result){
    result.addRow(row); 
  
  })


  query.on("end", function (result) {
    console.log(JSON.stringify(result));
    // if this is first time that user has watched this stream, add to db
    if (result.rows.length == 0){
      
      var query = db_client.query("INSERT into user2watched (user_id,stream_id) "+
        "values ($1,$2)",[user_id,stream_id]); 

      query.on("error",function(err){
        console.log("err" + err); 
        
      });

      query.on("end",function(){
        return callback(); 
      })
    }

    // user has watched this video before
    // incriment viewcount 
    else{ 
      console.log("here about to query"); 
      var query = db_client.query("UPDATE user2watched SET view_count = view_count + 1" +
        " WHERE user_id=$1 AND "
        + "stream_id=$2",[user_id,stream_id]); 

      query.on("error",function(err){
        console.log("error " + err); 
        return callback(); 
      });

      query.on("end", function () {
             
        //db_client.end();
        return callback();
                 
      }); 
    }          
  });
}


exports.getRelatedStreams = function(stream_id,language,callback){

  // get stream tags for stream with id stream_id
  var tag_ids = []; 
  var related_stream_ids = []; 
  var query = db_client.query("SELECT tag_id from stream2tag WHERE stream_id=$1",[stream_id]);

    query.on("row",function(row){
        tag_ids.push(row.tag_id); 
      });
 
    // got tag ids for stream with stream_id 
    query.on("end", function () {


      console.log("the tags " + tag_ids); 

      // if there are tags
      if(tag_ids.length > 0){


      // get other streams with these tags 
      var query_string = "SELECT DISTINCT stream_id from stream2tag where tag_id in (" + 
      tag_ids + ')';
      console.log("getrelated query string " + query_string); 
      
      var query=db_client.query(query_string); 

      query.on("error",function(err){
        console.log("err" + err); 
        return callback(); 
      });

      query.on("row",function(row){

        // omit this  stream from results 
        if(row.stream_id != stream_id ){
        related_stream_ids.push(row.stream_id); 
        }
      });
 
 

      query.on("end", function () {

        // get streams in same category as well ** TODO 
        console.log("the related stream ids " + related_stream_ids);      
       
        // get stream info to preview to frontend
        getStreamPreviews(related_stream_ids,language,function(streams){
        //db_client.end();
        return callback(streams);

        });               
      }); 
    }

    // stream has no tags
    else{
      callback({}); 
    }           
  });
}


exports.getFavourites = function(sess,user_id,language,callback){


  // if no user, get their vote from session

  if (user_id == null){

      getStreamPreviews(getSessionFavouritesIDs(sess),language,function(streams){
        console.log("DONE");
        return callback(streams); 
      })
  }

  else{

  // user has an account 

  var query = db_client.query("SELECT s.id,s.type,s.thumbnail,sl.title,sl.description FROM stream s" +
    " JOIN stream2lang sl on sl.id=s.id JOIN user2favourites uf on uf.stream_id = s.id" + 
    " WHERE uf.user_id=$1 and sl.language=$2", [user_id,language]);

  query.on("error",function(err){

    console.log(err); 
  })
  query.on("row", function (row,result) {
       result.addRow(row); 
    });
       
    query.on("end", function (result) {
         
     //db_client.end();
       return callback(result.rows);
             
    }); 

  }
}


exports.getPopularStreams = function(language,callback){

  var popular_stream_ids = []; 

  var query = db_client.query("select stream_id,sum(view_count) as total_views " +
  "from user2watched group by stream_id order by total_views desc");

  query.on("error",function(err){

    console.log(err); 
  })
  
  query.on("row", function (row,result) {
       popular_stream_ids.push(row.stream_id); 
    });
       
    query.on("end", function (result) {

      getStreamPreviews(popular_stream_ids,language,function(streams){

        
       return callback(streams);
      })
    }); 
  }


exports.getAdTitles= function(lang,callback){

	var ids_concat_titles = []; 

	// add new category to database 
	var query = db_client.query("SELECT s.id,sl.title FROM STREAM s JOIN STREAM2LANG sl "
    + " ON sl.id = s.id WHERE s.type='advertisement' and sl.language=$1", [lang]);

	query.on("row", function (row) {

			var ad_tag= row.id + ": " + row.title; 

			 ids_concat_titles.push(ad_tag); 		    
		});
			 
		query.on("end", function () {
				 
		 //db_client.end();
	     return callback(undefined,ids_concat_titles);
			       
		}); 
	}


exports.getAllStreamsOfType = function(type,language,callback){
	 

	 var query = db_client.query("SELECT s.*,sl.title,sl.description from STREAM s JOIN" +
	 		" STREAM2LANG sl ON sl.id = s.id WHERE s.type=$1 AND sl.language=$2" +
      " AND s.pub_start <= now() and s.pub_end > now()" ,[type,language]);
	
	 query.on("row", function (row, result) {
	     result.addRow(row);
	    
	 });
	 
	 query.on("end", function (result) {
		 
	    // db_client.end();
	    
	     return callback(undefined,result.rows);
	       
	 }); 
	 
};
	 
// getCategoryStreams

exports.getTypeStreamsOfCategory = function(type,language,category_id,
  callback){
		 

	var query = db_client.query("SELECT s.*,sl.title,sl.description" +
      " FROM STREAM s JOIN STREAM2CATEGORY sc ON sc.stream_id = s.id" +
      " JOIN CATEGORY c ON sc.category_id = c.id" +
      " JOIN STREAM2LANG sl ON sl.id = s.id" +
      " WHERE s.type=$1 AND sl.language=$2" +
      " AND s.pub_start <= now() AND s.pub_end > now()" +
      " AND c.id=$3", [type,language,category_id]);

	query.on("row", function (row, result) {
	    result.addRow(row);
		    
	});
		 
	query.on("end", function (result) {
			 
	 //db_client.end();
     return callback(undefined,result.rows);
		       
	}); 
	
};


exports.getTypeStreamsOfCategoryByCategoryID = function(type,language,category_id,callback){
		 

	var query = db_client.query("SELECT s.*,sl.title,sl.description FROM STREAM s" +
      " JOIN STREAM2CATEGORY sc ON sc.stream_id = s.id" +
			" JOIN CATEGORY c ON sc.category_id = c.id" +
			" JOIN CATEGORY2LANG cl ON cl.id = c.id" +
			" JOIN STREAM2LANG sl ON sl.id = s.id " +
			"WHERE s.type=$1 AND c.type=$1 AND sl.language=$2 AND cl.language=$2 " +
			"AND cl.id=$3", [type,language,category_id]);
	

	query.on("row", function (row, result) {
	    result.addRow(row);
		    
	});
		 
	query.on("end", function (result) {
			 
	 //db_client.end();
     return callback(undefined,result.rows);
		       
	}); 
	
};
	

// return true if stream with stream_id is favourited by user with user_id 
exports.checkIfFavourited= function(sess,user_id,stream_id,callback){

  // no account 

  if(user_id == null){
    // if no favourites or if stream not in favourites list 
    
    console.log("the favourites" + sess.favourites);

    if(!sess.favourites || sess.favourites.indexOf(stream_id) < 0 ){
      console.log("not favourited"); 
      return callback(false);
    }
    console.log("checkiffavourited favourited");
    return callback(true); 
  }


  // account 
  var query = db_client.query("SELECT * from user2favourites WHERE "
    + "user_id=$1 AND stream_id=$2", [user_id,stream_id]);
  

  query.on("row", function (row, result) {
      result.addRow(row);
        
  });
     
  query.on("end", function (result) {
       
  
  // customer has not favourited this stream
  if (result.rows.length==0){
    console.log("customer has not favourited this video");
    return callback(false);
  }

  // customer has favourited this stream 
  else{
     console.log("customer has favourited this video");
     return callback(true);
    }         
  }); 
} 



exports.getRecentlyWatched = function(sess,user_id,language,callback){

   // if no user, get their vote from session

  if (user_id == null){

    getStreamPreviews(getSessionWatchedIDs(sess),language,function(streams){
      
      return callback(streams); 
    })
  }

  else{

  // add new category to database 
  var query = db_client.query("SELECT s.id,s.type,s.thumbnail,sl.title,sl.description" +
    " FROM stream s" +
    " JOIN stream2lang sl on sl.id=s.id JOIN user2watched w on w.stream_id = s.id" + 
    " WHERE w.user_id=$1 and sl.language=$2 ORDER BY viewed_at desc LIMIT 10", [user_id,language]);

  query.on("error",function(err){

    console.log(err); 
  })

  query.on("row", function (row,result) {
       result.addRow(row); 
    });
       
  query.on("end", function (result) {
       
   //db_client.end();
     return callback(result.rows);
           
  });
 } 
}

	 
// getStreamByID

// LIVESTREAM  *** todo 

exports.getStreamCategories= function(stream_id,current_language,type,callback){

var query = db_client.query("SELECT s.id, cl.label FROM stream2category sc" +
 " JOIN stream s on sc.stream_id = s.id JOIN category c on c.id=sc.category_id" +
 " JOIN category2lang cl on cl.id = sc.category_id" +
 " WHERE s.id=$1 AND cl.language=$2 AND c.type=$3",[stream_id,current_language,type]);



query.on("row", function (row, result) { 
			
	    result.addRow(row);
		    
	});

query.on("error", function(err){
	console.log("error in getStreamcategories " + err);
	return callback(err,undefined);
})

query.on("end", function (result) {
			 
	// db_client.end();
		console.log("results from getStreamCategories model " + JSON.stringify(result.rows));
     return callback(undefined,result.rows);
		       
	});

}

exports.getStream = function(stream_id,current_language,stream_type,callback){
  getStream(stream_id,current_language,stream_type,function(err,stream){
    return callback(err,stream); 
  }); 
}


exports.getStreamAds = function(stream_id,current_language,callback){

var query = db_client.query("SELECT s.id," +
			" ad.title as ad_title,ad_data.video,ad.id as ad_id FROM STREAM s" +
			" JOIN stream2ad sa on sa.stream_id = s.id JOIN stream2lang ad on ad.id=sa.ad_id" +
			" JOIN advertisement ad_data on ad.id=ad_data.id WHERE s.id=$1 AND ad.language =$2",
	[stream_id,current_language]);

query.on("row", function (row, result) {
	    result.addRow(row);
		    
	});

query.on("end", function (result) {
			 
		
		console.log("results from getStreamAds model " + JSON.stringify(result.rows));
     return callback(undefined,result.rows);
		       
	});


}

// get non language stream data 
// get category data based on current language *** 


// getStreamData *** 

exports.getAllStreamTranslations= function(stream_id,callback){
	 
 
	var query = db_client.query("SELECT * from stream2lang where id=$1", [stream_id]);
	
	var map = new Object(); 

	query.on("row", function (row) {
			map[row.language] = row; 
			    
	});

	query.on("error", function (err) {
		console.log(err);   
		    
	});

 
	query.on("end", function () {
			 
	// db_client.end();
     return callback(undefined,map);
		       
	});
	
};


exports.getTagsForStream = function(stream_id,callback){

	var tags = []; 
	var query = db_client.query("SELECT label FROM tag t JOIN stream2tag "
		+ " st on st.tag_id=t.id WHERE st.stream_id=$1",
		[stream_id]);


	query.on("row", function (row, result) {
	   tags.push(row.label);
	});

		 
	query.on("end", function (result) {

		callback(tags); 

		}); 
}


exports.getCategoryIDByLabel= function(label,language,type,callback) {


  var query = db_client.query("SELECT c.id,cl.label from category2lang cl JOIN" +
    " category c on cl.id=c.id " +
  	" WHERE cl.language=$1 AND cl.label=$2 AND c.type=$3",
        [language,label,type]
         );

  query.on("row", function (row, result) {
      result.addRow(row);
        
    });

    query.on("error", function (err) {
      console.log(err)
        
    });
     
    query.on("end", function (result) {
      callback(undefined,result.rows[0].id); 
    });


}


exports.getCategoryLabelsForStream= function(stream_id,current_language,callback){

var labels = []; 
var query = db_client.query("SELECT cl.label FROM stream2category sc" +
 " JOIN stream s on sc.stream_id = s.id JOIN category c on c.id=sc.category_id" +
 " JOIN category2lang cl on cl.id = sc.category_id" +
 " WHERE s.id=$1 AND cl.language=$2",[stream_id,current_language]);


query.on("row", function (row, result) {
  labels.push(row.label) 
  });

query.on("end", function (result) {
       
  // db_client.end();
  console.log("results from getStreamCategories model " +
   JSON.stringify(labels));
  return callback(labels);
           
  });


}

// add time to playtime for user with user_id for stream with stream_id 
 exports.trackPlaytime = function(req,user_id,stream_id,time,callback){
  var sess = req.session; 

  cache.hset(user_id,stream_id,time,function(err,reply){
    console.log("cache err " + err); 
     //expire in a week
     cache.expireat(user_id, parseInt((+new Date)/1000) + 604800);

     sess.user.watch_queue[stream_id] = time;  

     callback(); 
  })

}



// ********HELPERS***********


// ie. en,fr,sp   becomes ('en'),('fr'),('sp')
function formatListForDBInsert(lst,callback){

  var insert_values = ""; 

  for (var i = 0 ; i < lst.length ; i ++ ){

    var s = "('";
    s = s + lst[i] + "')"
    // if not the last item
    if (i < (lst.length-1)){
      s = s + ","
    }
    insert_values = insert_values + s; 

  }

    if(i == lst.length){
    console.log("formatted insert values " + insert_values);
    callback(insert_values); 
  }


}


// LIMIT how much comes back **TODO 
// get stream title,description,id,thumbnail and type for list of streams with ids stream_ids and in language 
function getStreamPreviews(stream_ids,language,callback){

  if(stream_ids.length == 0 ){
    return callback({}); 
  }

   var query_string = "SELECT s.id,sl.title,sl.description,s.type,s.thumbnail from stream2lang sl" +
   " JOIN stream s on s.id=sl.id WHERE sl.language='" + language + "'" + 
   " AND s.id in (" + stream_ids + ")" 

  console.log("getstreampreviews query string " + query_string); 

  var query = db_client.query(query_string); 
  
  query.on("row", function (row, result) {
      result.addRow(row);
        
    });

    query.on("error", function (err) {
      console.log(err)
        
    });
     
    query.on("end", function (result) {
      console.log("DONE");
     
      callback(result.rows); 
    });

}


function getStream(stream_id,current_language,stream_type,callback){

  var stream = {}; 

  // get non-language stream info
  getStreamInfo(stream_id,stream_type,function(err,stream_info){

    stream = stream_info;
    console.log("the stream info " + JSON.stringify(stream));
    
    if (!stream){
      return callback(404,undefined)
    } 

     // get language stream info for current language
     getStreamTranslations(stream_id,current_language,function(stream_translations){
      // if results is none, try getting stream translations in default language 
      if(stream_translations == null){
         getStreamTranslations(stream_id,config.default_language,
          function(stream_translations){

            if(stream_translations != null ){
              stream.title = stream_translations.title;
              stream.description = stream_translations.description;
              }
             return callback(undefined,stream); 
          }); 

      }
      // got stream translations for current language
      else{
         stream.title = stream_translations.title;
         stream.description = stream_translations.description; 
        return callback(undefined,stream); 
      }

     })

  });

}



function getStreamInfo(stream_id,type,callback){

  var query;


  if (type == 'livestream'){
   
  query = db_client.query("SELECT s.id,s.type,s.thumbnail,s.pub_start,s.pub_end," +
      " s.featured,s.private,l.video FROM STREAM s" +
      " JOIN LIVESTREAM l on l.id=s.id" +
      " WHERE s.id=$1 AND s.pub_start <= now() AND s.pub_end > now()"
      , [stream_id]);

  }

  if (type == 'ondemand'){
   
  query = db_client.query("SELECT s.id,s.type,s.thumbnail,o.video,s.pub_start,s.pub_end,s.featured,s.private" +
      " FROM STREAM s" + 
      " JOIN ONDEMAND o on o.id=s.id" +
      " WHERE s.id=$1 AND s.pub_start <= now() AND s.pub_end > now()", 
      [stream_id]);

  }

  if (type == 'advertisement'){
   
  query = db_client.query("SELECT s.id,s.type,s.thumbnail,a.video" +
      " FROM STREAM s" +
      " JOIN advertisement a on a.id=s.id" +
      " WHERE s.id=$1", [stream_id]);

  }

  query.on("error",function(err){
    console.log("error in model " + err);
  });
  

  query.on("row", function (row, result) {
      result.addRow(row);
        
  });

     
  query.on("end", function (result) {

    // if results = null, try to get results in default language
    // since translations for this stream may not yet exist for current language  


     console.log("results from getStreamByID model " + JSON.stringify(result.rows));
     return callback(undefined,result.rows[0]);


       
  // db_client.end();
   
           
   });
};

function getStreamTranslations(stream_id,language,callback){

   var query = db_client.query("SELECT sl.title, sl.description FROM stream2lang sl" +
    " JOIN stream s on s.id=sl.id WHERE s.id=$1 AND sl.language=$2",[stream_id,language]);

  query.on("row", function (row, result) {
        result.addRow(row);
          
    });

  query.on("end", function (result) {
       
    return callback(result.rows[0]);
           
  });

}

function getSessionVote(sess,stream_id){
  console.log("the session " + JSON.stringify(sess));
  if(!sess.votes){
    sess.votes = {}
  }
  
  if(!sess.votes[stream_id]){
    return 0;
  }
  else{
    return sess.votes[stream_id]
  }

}


function getSessionFavouritesIDs(sess){
  
  if(!sess.favourites){
    sess.favourites= []
  }
  
  
  return sess.favourites; 

}




function getSessionWatchedIDs(sess){

   if(!sess.watched){
    sess.watched= []
  }
  
  
  return sess.watched; 

}













	 




	
	
