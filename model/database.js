/**
 * DATABASE CONN 
 */

var pg = require('pg');

var con_string = config.pg.con_string; 

var client = new pg.Client(con_string);

client.connect();


// SINGLETON DATABASE EXAMPLE 

/* var singleton = (function() {

  var instance; //Singleton Instance

  function init() {

    var _db; //Instance db object (private)
    
    //Other private variables and function can be declared here

    return {

      //Gets the instance's db object
      getDB: function() {
        return _db;
      },

      //Sets the instance's db object
      setDB: function(db) {
        _db = db;
      }
      
      //Other public variables and methods can be declared here
    };

  }

  return {
    //getInstance returns the singleton instance or creates a new one if
    //not present
    getInstance: function() {
      if (!instance) {
        instance = init();
      }

      return instance;
    }
  };

})();  

module.exports = singleton.getInstance();  */ 

module.exports = client; 
