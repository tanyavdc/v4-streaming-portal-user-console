// load all the things we need
	var LocalStrategy   = require('passport-local').Strategy;

	
	var db = require('./database.js');
	
	var flash = require('connect-flash');

  var moment = require('moment');

	
	var sess; 


	
	// expose this function to our app using module.exports
	module.exports = function(passport) {
	
	    // =========================================================================
	// passport session setup ==================================================
	// =========================================================================
	// required for persistent login sessions
	// passport needs ability to serialize and unserialize users out of session
	
	// used to serialize the user for the session
	passport.serializeUser(function(user, done) {

			var user_data = {id:user.id,email:user.email}
	    done(null, user_data);
	});
	
	// used to deserialize the user
	passport.deserializeUser(function(user, done) {
		
		done(null,user);
	  });

	// normal login 
	passport.use('user-local', new LocalStrategy({
	        // by default, local strategy uses username and password, 
	        // we will override with email
	        usernameField : 'email',
	        passwordField : 'password',
	       passReqToCallback : true // allows us to pass back the entire request to the callback
	    },function(req,email, password, done) {

        var sess = req.session; 

	    	
	    	// type = user ** TODO 
	    	var query = db.query("SELECT * from users WHERE email=$1 and "
	    		+ " type='user'", [email]);
	    	
	    	query.on("row", function (row, result) {
	    	    result.addRow(row);
	    		    
	    	});
	    		 
	    	query.on("end", function (result) {
	    			 
	    
	    		var rows = result.rows;
	    		
	    		 if (!rows.length) {
		 				
	            return done(null, false, req.flash('loginMessage', 'No user found.')); 
	                 // req.flash is the way to set flashdata using connect-flash
	           } 
			
						// if the user is found but the password is wrong
           var db_password_hash = rows[0].password; 
           var passwords_match = bcrypt.compareSync(password,db_password_hash); 
           if (!passwords_match){
          	 
              return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); 
               // create the loginMessage and save it to session as flashdata
           }

           // all is well, return successful user
           
        	 sess.user.logged_in = true;
           return done(null, rows[0]);				    		       
	    		}); 
	    	}
	    ));


	// stripe login 
	passport.use('stripe-local', new LocalStrategy({
	        // by default, local strategy uses username and password, 
	        // we will override with email
	        usernameField : 'email',
	        passwordField : 'email',  // no password field for stripe 
	       passReqToCallback : true // allows us to pass back the entire request to the callback
	    },function(req,email, password, done) {

        var sess = req.session; 

        var customer; 
	    
	    	var query = db.query("SELECT u.id,u.email,c.stripe_id,c.last4digits,"+
         "c.subscription_plan,c.subscription_id,c.expiry FROM users u JOIN credentials" +
         " c on c.customer_id=u.id" +
         " WHERE u.email=$1 and u.type='customer'", [email]);
	    	
	    	query.on("row", function (row, result) {
	    	    result.addRow(row);
	    		    
	    	});
	    		 
	    	query.on("end", function (result) {

	    		customer = result.rows[0]; 
	    
	    		var rows = result.rows;
          
           if (!rows.length) {
            
		 				
	            return done(null, false, req.flash('loginMessage', 'No user found.')); 
	                 // req.flash is the way to set flashdata using connect-flash
	         } 
            var stripe_id = customer.stripe_id;
            var expiry = customer.expiry; 

            var today = moment();
            console.log("today " + today);
            console.log("Expiry " + expiry); 

            // or login anyways but without access?? **TODO
            if(stripe_id == null){
              return done(null,false, req.flash('loginMessage', 'No user found.'))
            }

            if(moment(today).isSameOrAfter(expiry)){
              sess.user.plan_expired = true;
              return done(null,false, req.flash('loginMessage', 'Your plan has expired'))
            }


           // here if stripe id is good and plan is still active
           // set user
           sess.user = customer; 
           // format expiry
           sess.user['expiry'] = moment(expiry).format('YYYY-MM-DD HH:MM') 
        	 sess.user.logged_in = true;
           console.log("the tough customer " + JSON.stringify(customer))
           return done(null, customer);				    		        
        })
	    }
	 ));
};

	    
	   

