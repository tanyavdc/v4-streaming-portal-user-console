var pg = require('pg');
var db_client = require('./database');
var cache = require('./cache');

var stripe = require('stripe')('sk_test_TZo04WKusvlZNT2Ipyo5XWPt');

var moment = require('moment');

/*server declaration
...
...
*/

require('jquery');


// CACHE 


// vote = 1 for upvote, -1 for downvote, 0 for unvote 
// user id is null if account is disabled 

exports.vote = function(vote,stream_id,user_id,callback){


  // clear old vote

  clearOldVote(vote,stream_id,user_id,function(){

     var query = db_client.query("INSERT into votes values($1,$2,$3)",
      [stream_id,vote,user_id]);

      query.on("end", function () {

      return callback(undefined); 
    
     });

      query.on("error",function(err){
        return callback(err); 
      })
  }); 
}; 




exports.getSettings = function(callback){

  var data = {}; 

  // cache hgetALl  ?

  cache.hgetall('domain',function(err,reply){

    console.log("cache reply " + reply);
    console.log("cache reply " + JSON.stringify(reply));

    // set to locals * 
   
    callback(reply);
  })
 
}


// edit stripe account 
exports.editPaymentProfile = function(user_id,stripe_id,req,callback){

  var edited_credit = req.body['edited-creditcard'];
  if(!edited_credit){
    return callback(false,null);
  }

  var sess = req.session; 

  var new_token = req.body['stripe-token'];
  var new_email = req.body['email'];

  console.log("the new token " + new_token); 

  var edited_user; 

  // details for db insert
  var last4digits;

  
  stripe.customers.update(stripe_id, {
  email: new_email,
  source: new_token   // updated credit card 
}, function(err, customer) {

  edited_user = customer; 
  new_token = customer.default_source; 
  last4digits = customer.sources.data[0].last4;

  console.log("the edited customer " + JSON.stringify(customer));

  
  var query = db_client.query("UPDATE credentials SET credit_token=$1" +
  ",last4digits=$2 WHERE stripe_id=$3 AND customer_id=$4"
  ,[new_token,last4digits,stripe_id,user_id]); 

    query.on("end", function (result) {

      // update user details stored in session
      updateSessionObj(sess.user,{last4digits:last4digits});
      console.log("here and the session is " + JSON.stringify(sess)); 

      return callback(true); 
      });
  });
}

exports.changePaymentPlan = function(user_id,subscription_id,req,callback){

  var sess = req.session; 

  // check whether or not user changed their plan in the form
  var edited_plan = req.body['edited-plan'];
  if(!edited_plan){
    return callback(false);
  }

  var new_plan = req.body['plan']; 
  console.log("the new plan " + new_plan);


  // update subscription
  stripe.subscriptions.update(subscription_id, {
    plan: new_plan
  }, function(err, subscription) {

    console.log("the subscription " + JSON.stringify(subscription)); 

    plan_expiry = moment.unix(subscription.current_period_end)
        .format("YYYY-MM-DD HH:mm");

    // update db info 

     var query = db_client.query("UPDATE credentials SET subscription_plan=$1,expiry=$2" +
      " WHERE subscription_id=$3 AND customer_id=$4"
    ,[new_plan,plan_expiry,subscription_id,user_id]); 

    // ERRR plan DNE ** 

    query.on("error", function (err) {
      console.log(err);
      return callback(false); 
    });

    query.on("end", function (result) {

      // update user details stored in session
     updateSessionObj(sess.user,{expiry:plan_expiry,subscription_plan:new_plan}); 

      return callback(true); 
    });
  });
}


exports.editEmail = function(user_id,new_email,sess,callback){
 

  // update entry in users

 var query = db_client.query("UPDATE users SET email=$1" +
    " WHERE id=$2"
  ,[new_email,user_id]); 

  // ERRR plan DNE ** 

  query.on("end", function (result) {

    // update user details stored in session
   updateSessionObj(sess.user,{email:new_email}); 
   // update passport
   sess.passport.user.email = new_email; 

    return callback(true); 
  }); 
};


// check if user entered password matches
// used to validate user when they edit their account
// ensuring their old password matches their password 
exports.validateUser = function(user_id,password,callback){


  var query = db_client.query("SELECT * FROM users WHERE id=$1 AND password=$2"
    ,[user_id,password]); 

  query.on("row",function(row,result){
    result.addRow(row); 
  })

  query.on("error",function(err){
   
    return callback(false); 
  });

  query.on("end", function (result) {

    // if no user found 
    if(!result.rows.length){
      // wrong password
      console.log("wrong credentials for " + user_id + "for pass " + password);
      return callback(false); 
    }

    // user found
    else{
      return callback(true); 
    }
  })
}


exports.editAccount = function(user_id,new_pass,req,callback){

   // get stripe id 
  var sess = req.session;

  var new_email = req.body['email']; 

  // callback with err if old password doesnt match db passwor

   var query = db_client.query("UPDATE users SET email=$1,password=$2" +
  " WHERE id=$3",[new_email,new_pass,user_id]);

  query.on("end", function (result) {

    // update session details
    updateSessionObj(sess.user,{email:new_email})
    sess.passport.user.email = new_email; 

    return callback(undefined);
           
  }); 
}


exports.favouriteStream = function(user_id,stream_id,callback){

  var query = db_client.query("INSERT into user2favourites (user_id,stream_id) "
    + "values ($1,$2)",[user_id,stream_id]); 

  query.on("error",function(err){
    // stream DNE 
    // stream may have been removed by admin while user was favouriting it
    return callback(err); 
  });

  query.on("end", function () {
         
    //db_client.end();
    return callback(undefined);
             
  }); 

}

exports.unfavouriteStream = function(user_id,stream_id,callback){
  console.log("unfavouriting stream in model");


  var query = db_client.query("DELETE from customer2favourites WHERE cust_id=$1 "
    + "AND stream_id=$2",[user_id,stream_id]); 

  query.on("end", function () {
         
    return callback();
             
  }); 

}

// normal signup
// 'account' type account 
exports.signup = function(email,hashed_password,callback){
 var query = db_client.query("INSERT into users (email,password,type) values"+
    " ($1,$2,'user')",
    [email,hashed_password]); 

   query.on("row",function(row,result){
    result.addRow(row); 
   })

   query.on("end",function(result){

    return callback(undefined); 
  });
}

// signups 

exports.stripeSignup = function(token,email,plan,callback){

  var user_id; 

  // for db insert 
  var stripe_id; 
  var last4digits;
  var plan_expiry;
  var credit_token; 
  var subscription_id;  

    // Create a Customer:
  stripe.customers.create({
    email: email,
    source: token,
  }).then(function(customer) {

    stripe_id = customer.id; 

    credit_token = customer.default_source; 
    last4digits = customer.sources.data[0].last4; 

    console.log(credit_token + " and " + last4digits); 

    // YOUR CODE: Save the customer ID and other info in a database for later.
    // insert into db : default id (not this one!!), email, token,
    // subscription type, last 4 digits
     var query = db_client.query("INSERT into users (email,type) values"+
      " ($1,'customer') RETURNING id",
      [email]); 

     query.on("row",function(row,result){
      result.addRow(row); 
     })

      query.on("error",function(err){

        console.log("the error" + err);
        return callback(err); 
     })


     query.on("end",function(result){

      user_id = result.rows[0].id; 
      console.log("the plan " + plan); 

      stripe.subscriptions.create({
        customer: customer.id,
        plan: plan
      }, function(err, subscription) {
          
        subscription_id = subscription.id; 
        plan_expiry = moment.unix(subscription.current_period_end)
        .format("YYYY-MM-DD HH:mm");

       // subscription expiry ** TODO 
        var query = db_client.query("INSERT into credentials" +
        " (customer_id,credit_token,last4digits,subscription_plan," +
        " subscription_id,stripe_id,expiry) values"
        + " ($1,$2,$3,$4,$5,$6,$7)",[user_id,credit_token,last4digits,
        plan,subscription.id,stripe_id,plan_expiry])

         query.on("error",function(err){
          console.log("the error " + err); 
          return callback(err); 
        });

        query.on("end",function(){
          return callback(undefined); 
        });
      }) 
    });
  });
}


//** HELPERS ** 

function clearOldVote(vote,stream_id,user_id,callback){

  // account type is none
  if(user_id == null){
    return callback();
  }
  else{
   var query = db_client.query("DELETE from votes where user_id=$1 AND stream_id=$2",
    [user_id,stream_id]); 

    query.on("end", function () {
      return callback();
    });
  }
}


// updates or creates key in session user object 
function updateSessionObj(sess_obj,updated_details){
  
  for (var key in updated_details) {
    sess_obj[key] = updated_details[key]
  }
}

